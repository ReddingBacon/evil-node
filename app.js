var express = require('express')
var app = express()
var child_process = require('child_process');

// This is used for the template injection function
app.set('view engine', 'ejs');


// LOGGING
// Good security requires that all requests are logged.
var logger = function (req, res, next) {
  console.log(Date.now() + ": HTTP Request: " + req.url)
  next()
}


app.use(logger)


// This is not a recommended web app configuration
app.use(function(req, res, next) {
	// Allows CORS requests:
	res.header('Access-Control-Allow-Origin', '*');
	next();
});


// Home page
app.get('/', function (req, res) {
  var message = req.query['message']
  if (message == null) {
    message = {message: "Welcome to Evil Node!"}
  } else {
    message = {message: ""+message}
  }
  res.render('index', message );
});


// Get the goat image
app.get('/img/:src', function (req, res) {
console.log("GET IMG: "+ req.params['src'])
  res.sendFile(__dirname + '/img/'+req.params['src'])
});


// COMMAND INJECTION
// Executes any and every command passed from the client.  BE CAREFUL!!
// Users can send what ever commands they want, and they will be executed
// with the priveleges of the account running the Node server.
//
// Example: http://localhost:9666/ls -; pwd;
// This will run the 'ls' command and then 'pwd'.  They could even add
// 'rm -rf'  which will recursively delete all files and folders under
// the current directory.
app.get("/cmd/", function (req, res) {
  var cmd = req.query['cmd'];
  console.log("Executing cmd: '"+cmd+"'");  // log injection again

  if(cmd == null || cmd == "") {
    cmd = "ls"
  }

  child_process.exec(cmd, function(err, data){
    if(err) {
      console.log(err);
      res.render('cmdInjection',{cmd: cmd, cmd_results: err});
    } else {
			console.log(data);
      res.render('cmdInjection',{cmd: cmd, cmd_results: data});
		}
  });
});


// LOG INJECTION
// Technically, there's log injection in most of the functions in this file!
// :-D
app.get("/log/", function (req, res) {
  var log_msg = req.query['log'];
  console.log("The user entered: '"+log_msg+"'")
  res.render('logInjection',{log_msg: log_msg});
});


// REFLECTED XSS vulnerability
app.get('/xss', function(req, res, next) {
	console.log('GET /xss');
	console.log(req.query);
	//res.send('I\'ll try to remember that..');
  res.render('xss')
});


// REGEX DOS and INJECTION
app.get('/regex', function(req, res) {
  console.log("REGEX Injection");

  var key = req.query['user_input'];
  console.log("user input: "+key);

  // BAD: Unsanitized user input is used to construct a regular expression
  var re = new RegExp("\\b" + key + "=(.*)\n");
  var result = re.test("Hello");
  console.log("RegEx result: "+result)
  res.render('regex', {regex: "RegEx return value: "+ result })
});


// TEMPLATE INJECTION
app.get("/ti/", function (req, res) {
  console.log("Template Inection")

  var user_input = req.query['user_input'];
  console.log("user_input = '"+user_input+"'") // This can be log inj as well

  res.render('templateInjection', {user_data: user_input})
});


// Listen on a semi-evil port
app.listen(9666);
