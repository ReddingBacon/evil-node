FROM node:14

COPY ./ ./

RUN npm install

EXPOSE 9666

CMD [ "node" , "app.js"]
