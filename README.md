# evil-node

> **Do not run evil-node on your local system or any environment you care about.**
> **Only run in containers or VMs.**

**evil-node** is a minimal, vulnerable Node JS web application for testing the
accuracy of security scanners _quickly_.  It contains common coding vulnerabilities
for assessing SAST tools and uses known-vulnerable modules that SCA/Dependency
Scanning tools should detect.  DAST tools should also be able to find many of
the vulnerabilities.

Larger more well-known vulnerable application projects, such as Web Goat or Node Goat,
can be used for more detailed analysis of security scanners. **evil-node** is much
smaller, much faster to scan, and won't impact LoC-based licensing tools.  If a scanner
doesn't find most of these vulnerabilities it's probably not a good security scanner,
and you can quickly move on to analyzing other tools.

**evil-node** is a basic, functioning web application that can also be used as an
educational tool for demonstrating common software vulnerabilities.  

## Vulnerabilities In evil-node
### Coding Errors (SAST)
At a minimum the following coding vulnerabilities exist:
* Command Injection
* Log Injection
* Template Injection
* RegEx DOS and Injection
* Reflected XSS

### Known Vulnerable Libraries In evil-node Modules (SCA/Dependency Scanning)
Findings should be at a _mininum_ the following:
* uglify-js
* morgan

## Running evil-node
The primary use of this app for quickly assessing the viability of security scanners,
but you can run it and attempt to exploit the vulnerabilities.  Do so using Docker.

`git clone https://gitlab.com/ReddingBacon/evil-node.git`

`cd evil-node`

`docker build . -t evil-node:latest`

`docker run --rm -d -p 9666:9666 evil-node:latest`


Go to http://localhost:9666/
